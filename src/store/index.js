import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let tasksFromLocalStorage = JSON.parse(window.localStorage.getItem('tasks'));

export default new Vuex.Store({
  state: {
    tasks: tasksFromLocalStorage ? tasksFromLocalStorage :[
      {
        description: 'Buy a new kettle',
        author: 'Violetta',
        timestamp: '26.01.2021 22:47',
        isDone: false
      },
      {
        description: 'Ring to the grandma',
        author: 'mama',
        timestamp: '26.01.2021 22:47',
        isDone: false
      },
      {
        description: 'Go to the gym',
        author: 'Violetta',
        timestamp: '26.01.2021 22:47',
        isDone: false
      },
      {
        description: 'Return the book to the library',
        author: 'Violetta',
        timestamp: '26.01.2021 22:47',
        isDone: false
      }
    ]
  },
  mutations: {
    removeTask(state, taskDescription) {
      this.state.tasks = this.state.tasks.filter(task => task.description !== taskDescription);
      this.commit('saveTasksToLocalStorage');
    },
    addTask(state, task) {
      state.tasks.unshift(task);
      this.commit('saveTasksToLocalStorage');
    },
    switchDone(state, task) {
      task.isDone=!task.isDone;
      state.tasks.sort((a, b) => a.isDone - b.isDone);
    },
    moveTaskUp(state, task) {
      let indexOfMovingTask = state.tasks.indexOf(task);
      if(indexOfMovingTask > 0 && !task.isDone) {
        state.tasks.splice(indexOfMovingTask, 1);
        state.tasks.splice(indexOfMovingTask - 1 , 0, task);
      }
    },
    moveTaskDown(state, task) {
      let indexOfMovingTask = state.tasks.indexOf(task);
      if( state.tasks[indexOfMovingTask + 1] && !state.tasks[indexOfMovingTask + 1].isDone) {
        state.tasks.splice(indexOfMovingTask, 1);
        state.tasks.splice(indexOfMovingTask + 1 , 0, task);

      }
    },
    saveTasksToLocalStorage(state) {
      window.localStorage.setItem('tasks', JSON.stringify(state.tasks));
    }

    
  },
  actions: {
  },
  modules: {
  }
})
